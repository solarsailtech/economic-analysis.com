<?php

	// Negatory to everyone who does a direct request
	if (empty($_POST))
		die('Invalid request.');

	// Recaptcha
	require_once('recaptchalib.php');
	$privatekey = '6LfiStoSAAAAAFbn1UVoudI0fAqK3CqOT0YJzrZa';
	$resp = recaptcha_check_answer ($privatekey,
								$_SERVER['REMOTE_ADDR'],
								$_POST['recaptcha_challenge_field'],
								$_POST['recaptcha_response_field']);

	if (!$resp->is_valid)
		// What happens when the CAPTCHA was entered incorrectly
		die ('Sorry, but the reCAPTCHA wasn&#39;t entered correctly. Please <a href="javascript: void()" onclick="window.history.go(-1)">go back</a> and try it again.');
	
	// Send email notification
	//$from = "From: form@economic-analysis.com\r\n";
	$from = 'Mailgun Sandbox <postmaster@sandboxd72acc4e005d4e8baa50e98fb8191d3f.mailgun.org>';
	
	$to = join(',', array(
		'richard@economic-analysis.com',
		'eric@solarsailtech.com'
	));
	//$to = 'eric@solarsailtech.com';
	//$to = 'richard@economic-analysis.com';
	
	$subject = 'Information Request';
	
	$body = join("\n", array(
		'Company: ' . $_POST['company'],
		'Contact: ' . $_POST['contact'],
		'Address: ' . $_POST['address'],
		'   City: ' . $_POST['city'],
		'  State: ' . $_POST['state'],
		'    Zip: ' . $_POST['zip'],
		'Country: ' . $_POST['country'],
		'  Phone: ' . $_POST['phone'],
		'    Fax: ' . $_POST['fax'],
		'  Email: ' . $_POST['emailrequesting'],
		'Request: ' . $_POST['requested']
	));
	
	//var_dump($from, $to, $subject, $body);
	//die();
	
	# Send email
	require_once 'vendor/autoload.php';
	use Mailgun\Mailgun;

	# Instantiate the client.
	$domain = "sandboxd72acc4e005d4e8baa50e98fb8191d3f.mailgun.org";
	$client = new \Http\Adapter\Guzzle6\Client();
	$mailgun = new \Mailgun\Mailgun('key-43f2dcf583049c8595493986212c8159', $client);

	# Make the call to the client.
	$result = $mailgun->sendMessage("$domain",
                  array('from'    => $from,
                        'to'      => $to,
                        'subject' => $subject,
                        'text'    => $body));
	
	header('Location: redirect.html');
?>