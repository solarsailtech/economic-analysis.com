#!/usr/bin/perl
#
# mailer.cgi 1997-03-29
# kurt@crosslink.net
#

use CGI::ErrorWrap;
use CGI::Base;
use CGI::Form;
use Mail::Send;

SendHeaders();

$query = new CGI::Form;
$msg = new Mail::Send(To=>$query->param('_to'),
		      Subject=>$query->param('_subject'));
$fh = $msg->open;
foreach $key ($query->param) {
    next if $key =~ /_/;
    $value = join(' ', $query->param($key));
    print $fh "$key: $value\n";
}
$fh->close;

print <<EOF;
<head>
<title>Thank you!</title>
</head>
<body>
Please use your browser's "back" button to return.
</body>
EOF
