
<?php
	if ($_SERVER['REQUEST_METHOD'] != 'POST')
		die('Sorry, but direct access to this page is not allowed.');
		
	require_once('recaptchalib.php');
	$privatekey = "6LfiStoSAAAAAFbn1UVoudI0fAqK3CqOT0YJzrZa";
	$resp = recaptcha_check_answer ($privatekey,
								$_SERVER["REMOTE_ADDR"],
								$_POST["recaptcha_challenge_field"],
								$_POST["recaptcha_response_field"]);

	if (!$resp->is_valid) {
		// What happens when the CAPTCHA was entered incorrectly
		die ('The reCAPTCHA wasn&#39;t entered correctly. Please <a href="javascript: void()" onclick="window.history.go(-1)">go back</a> and try it again. (reCAPTCHA said: ' . $resp->error . ')');
	} else {
		// Your code here to handle a successful verification
?>
<html>
    <body onload="document.forms[0].submit()">
		<form action="/cgi-bin/formmail.pl" method="post">
			<?php foreach( $_POST as $key => $val ) : ?>
                <input type="hidden" name="<?php echo htmlspecialchars( $key, ENT_COMPAT, 'UTF-8' ) ?>" value="<?php echo htmlspecialchars( $val, ENT_COMPAT, 'UTF-8' ) ?>">
            <?php endforeach; ?>
		</form>
    </body>
</html>
<?php } ?>

