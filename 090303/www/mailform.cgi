#!/usr/bin/perl

# 29 Aug 1995 RRV:
# 
# mail-form.pl: take the From infro from ecoform.htm and 
#         Email it to richard@crosslink.net
#
# ------------------------------------------------------------
# Form-mail.pl, by Reuven M. Lerner (reuven@the-tech.mit.edu).
#
# Last updated: January 23, 1996
#
# ------------------------------------------------------------
# This package is Copyright 1994 by The Tech. 

# Form-mail is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2, or (at your option) any
# later version.

# Form-mail is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Form-mail; see the file COPYING.  If not, write to the Free
# Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
# ------------------------------------------------------------

# Define fairly-constants


# Print out a content-type for HTTP/1.0 compatibility
# NOTE: this is required for the client to know what type of input
#       it is/will be receiving.
# print "Content-type: text/htm\n\n";
print "Location: http://www.economic-analysis.com/index.shtml\n\n";

# print "this is a test...";
# exit;

# Print a title and initial heading
# print "<Head><Title>Home Page Automatic Update</Title></Head>";
# print "<Body><H1>Home Page Automatic Update</H1>";

# Get the input
# read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});

# Get the input form the FORM
if ($ENV{'REQUEST_METHOD'} eq 'POST') {
  read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
} else {
  $buffer = $ENV{'QUERY_STRING'};
}

# Split the name-value pairs
@pairs = split(/&/, $buffer);

foreach $pair (@pairs)
{
    ($name, $value) = split(/=/, $pair);

    # Un-Webify plus signs and %-encoding
    # need to remove any HTML markups also: has not been completed yet! 
    # 
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;

    # Stop people from using subshells to execute commands
    # Not a big deal when using sendmail, but very important
    # when using UCB mail (aka mailx).
    $value =~ s/[|<>]//g; 

    $FORM{$name} = $value;
}

@printem = split(/\,/, $FORM{"printem"});
$numprints = 0;
foreach $pe (@printem)
{
  $PRINTEM{$numprints} = $pe ;
  $numprints = $numprints + 1;
}

## Uncomment the following for debugging
#
# open( FORM_ARRAY, ">/tmp/form.array" ) || die "Ugly death -- form.array" ;
# 
# foreach $var1 (keys(%FORM))
#   {
#   print FORM_ARRAY $var1, " = ", $FORM{$var1}, "\n" ;
#   } 
# 
# close(File_Array);

##

# send out the form info to the account

if ($FORM{'email'})
   {
   $mailfrom = $FORM{'email'};
   }
else
   {
   $mailfrom = "httpd\@crosslink.net";
   }

$to = "richard\@crosslink.net";
# $to = "herrin@why.com";

open( MAIL, "| /usr/lib/sendmail -f $mailfrom $to" );
print MAIL "From: $mailfrom (Web Server)\n";
print MAIL "To: $to\n";
print MAIL "Subject: Information Request: ecoform.htm\n\n";

$index = 0;
while ($index < $numprints)
  {
    print MAIL $PRINTEM{$index}, " = ", $FORM{$PRINTEM{$index}}, "\n" ;
    $index = $index + 1;
  }

#foreach $var1 (keys(%FORM))
#   {
#   print MAIL $var1, " = ", $FORM{$var1}, "\n" ;
#   } 

close(MAIL);




